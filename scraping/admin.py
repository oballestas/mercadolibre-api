from django.contrib import admin
from scraping.models import Laptop


# Register your models here.

@admin.register(Laptop)
class LaptopAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'price', 'url')
