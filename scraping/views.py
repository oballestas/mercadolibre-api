from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets
from scraping.models import Laptop
from scraping.serializers import LaptopSerializer


class LaptopViewSet(viewsets.ModelViewSet):
    queryset = Laptop.objects.all()
    serializer_class = LaptopSerializer
    # permission_classes = [permissions.IsAuthenticated]
