from django.urls import include, path
from rest_framework import routers

from scraping.views import LaptopViewSet

router = routers.DefaultRouter()
router.register(r'laptops', LaptopViewSet)

urlpatterns = [
    path('', include(router.urls))
]
