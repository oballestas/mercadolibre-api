from django.db import models


# Create your models here.

class Laptop(models.Model):
    name = models.TextField()
    price = models.FloatField()
    url = models.URLField()

    def __str__(self):
        return self.name
